package uz.devops.service.impl;

import org.apache.catalina.connector.Connector;
import org.apache.catalina.connector.Request;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;
import uz.devops.config.TestConfig;
import uz.devops.pm.parser.domain.PmParser;
import uz.devops.pm.parser.domain.enumeration.MethodType;
import uz.devops.pm.parser.domain.enumeration.ModeType;
import uz.devops.pm.parser.domain.enumeration.ProtocolType;
import uz.devops.pm.parser.domain.enumeration.RequestType;
import uz.devops.pm.parser.helper.PmParserHelper;
import uz.devops.pm.parser.service.dto.PmParserDTO;
import uz.devops.pm.parser.service.dto.RequestDTO;
import uz.devops.pm.parser.service.impl.PmParserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TestConfig.class})
public class PmParserServiceImplTest {

    @Autowired
    private PmParserServiceImpl pmParserService;

    @MockBean
    private PmParserHelper pmParserHelper;

    @Before
    public void setUp() {
        //Do something on starting
    }

    @After
    public void tearDown() {
        //Do something on finishing
    }

    @Test
    public void testCreatePmParserPostman() throws IOException, JSONException {
        List<PmParser> pmParserList = new ArrayList<>();
        PmParser pmParser = new PmParser();
        pmParser.setUrl("http://localhost:9000/api/gateway-params?page=0&size=20");
        pmParser.setType(RequestType.PAY);
        pmParser.setToken("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTY3MjEyMDM1M30.edsjiQUyFfSRE3UZunt8jB52smU04uQr5A2dagdjbMN-nApEIeTFHjNDhBwLAmMOLWEyHzTCsT7yo5vHklDt7g");
        pmParser.setHttpMethodType(MethodType.GET);
        pmParser.setHeaders("");
        pmParser.setProtocol(ProtocolType.REST);
        pmParser.setRequestMediaType("application/json");
        pmParser.setResponseMediaType("application/json");
        pmParser.setActive(true);
        pmParser.setMode(ModeType.PROD);
        pmParserList.add(pmParser);
        HttpServletRequest request = new Request(new Connector());
        MultipartHttpServletRequest multipartHttpServletRequest =
            new DefaultMultipartHttpServletRequest(request);

        Mockito.doReturn(pmParserList).when(pmParserHelper)
            .gatewayParamFromPostmanCollection(
                ArgumentMatchers.any(),
                ArgumentMatchers.anyString()
            );

        List<PmParserDTO> save = pmParserService.save(multipartHttpServletRequest,
            "");
        printObjToConsole("Create PmParser Postman", save);
        assertNotNull(save);
    }

    @Test
    public void testCreatePmParser() {
        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setUrl("http://localhost:8080/api/kun.uz");
        requestDTO.setType(RequestType.PAY);
        requestDTO.setUsername("username");
        requestDTO.setPassword("password");
        requestDTO.setHttpMethodType(MethodType.GET);
        requestDTO.setHeaders("");
        requestDTO.setProtocol(ProtocolType.REST);
        requestDTO.setRequestMediaType("application/json");
        requestDTO.setResponseMediaType("application/json");
        requestDTO.setActive(true);
        requestDTO.setMode(ModeType.PROD);
        PmParserDTO save = pmParserService.save(requestDTO);
        printObjToConsole("Create PmParser", save);
        assertNotNull(requestDTO);
    }

    private void printObjToConsole(String message, Object obj) {
        System.out.println(message + " -> " + obj);
    }
}
