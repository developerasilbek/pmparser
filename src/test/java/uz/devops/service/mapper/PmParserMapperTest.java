package uz.devops.service.mapper;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import uz.devops.config.TestConfig;
import uz.devops.pm.parser.domain.PmParser;
import uz.devops.pm.parser.domain.enumeration.MethodType;
import uz.devops.pm.parser.domain.enumeration.ModeType;
import uz.devops.pm.parser.domain.enumeration.ProtocolType;
import uz.devops.pm.parser.domain.enumeration.RequestType;
import uz.devops.pm.parser.service.dto.PmParserDTO;
import uz.devops.pm.parser.service.mapper.PmParserMapper;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TestConfig.class})
public class PmParserMapperTest {

    @Autowired
    private PmParserMapper pmParserMapper;

    @Test
    public void testDtoToPmParser() {
        List<PmParserDTO> pmParserDTOList = new ArrayList<>();
        PmParserDTO pmParserDTO = new PmParserDTO();
        pmParserDTO.setId(1L);
        pmParserDTO.setUrl("http://localhost:9000/api/gateway-params?page=0&size=20");
        pmParserDTO.setType(RequestType.PAY);
        pmParserDTO.setToken("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTY3MjEyMDM1M30.edsjiQUyFfSRE3UZunt8jB52smU04uQr5A2dagdjbMN-nApEIeTFHjNDhBwLAmMOLWEyHzTCsT7yo5vHklDt7g");
        pmParserDTO.setHttpMethodType(MethodType.GET);
        pmParserDTO.setHeaders("");
        pmParserDTO.setProtocol(ProtocolType.REST);
        pmParserDTO.setRequestMediaType("application/json");
        pmParserDTO.setResponseMediaType("application/json");
        pmParserDTO.setActive(true);
        pmParserDTO.setMode(ModeType.PROD);
        pmParserDTOList.add(pmParserDTO);
        List<PmParser> pmParserList = pmParserMapper.toEntity(pmParserDTOList);
        printObjToConsole("DTO to PmParser", pmParserList);
        Assertions.assertNotNull(pmParserList);
    }

    @Test
    public void testPmParserToDto() {
        List<PmParser> pmParserList = new ArrayList<>();
        PmParser pmParser = new PmParser();
        pmParser.setUrl("http://localhost:8080/api/kun.uz");
        pmParser.setType(RequestType.PAY);
        pmParser.setUsername("username");
        pmParser.setPassword("password");
        pmParser.setHttpMethodType(MethodType.GET);
        pmParser.setHeaders("");
        pmParser.setProtocol(ProtocolType.REST);
        pmParser.setRequestMediaType("application/json");
        pmParser.setResponseMediaType("application/json");
        pmParser.setActive(true);
        pmParser.setMode(ModeType.PROD);
        pmParserList.add(pmParser);
        List<PmParserDTO> pmParserDTOList = pmParserMapper.toDto(pmParserList);
        printObjToConsole("PmParser to DTO", pmParserDTOList);
        Assertions.assertNotNull(pmParserDTOList);
    }

    private void printObjToConsole(String message, Object obj) {
        System.out.println(message + " -> " + obj);
    }

}
