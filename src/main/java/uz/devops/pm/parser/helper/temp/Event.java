package uz.devops.pm.parser.helper.temp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Event {

    private String listen;
    private Script script;

    @JsonProperty("listen")
    public String getListen() {
        return listen;
    }

    @JsonProperty("listen")
    public void setListen(String value) {
        this.listen = value;
    }

    @JsonProperty("script")
    public Script getScript() {
        return script;
    }

    @JsonProperty("script")
    public void setScript(Script value) {
        this.script = value;
    }

    @Override
    public String toString() {
        return "Event{" + "listen='" + listen + '\'' + ", script=" + script + '}';
    }
}
