package uz.devops.pm.parser.helper.temp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProtocolProfileBehavior {

    private boolean disableBodyPruning;

    @JsonProperty("disableBodyPruning")
    public boolean getDisableBodyPruning() {
        return disableBodyPruning;
    }

    @JsonProperty("disableBodyPruning")
    public void setDisableBodyPruning(boolean value) {
        this.disableBodyPruning = value;
    }

    @Override
    public String toString() {
        return "ProtocolProfileBehavior{" + "disableBodyPruning=" + disableBodyPruning + '}';
    }
}
