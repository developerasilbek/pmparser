package uz.devops.pm.parser.helper.temp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Item {

    private String name;
    private List<Event> event;
    private ProtocolProfileBehavior protocolProfileBehavior;
    private Request request;
    private List<Object> response;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String value) {
        this.name = value;
    }

    @JsonProperty("event")
    public List<Event> getEvent() {
        return event;
    }

    @JsonProperty("event")
    public void setEvent(List<Event> value) {
        this.event = value;
    }

    @JsonProperty("protocolProfileBehavior")
    public ProtocolProfileBehavior getProtocolProfileBehavior() {
        return protocolProfileBehavior;
    }

    @JsonProperty("protocolProfileBehavior")
    public void setProtocolProfileBehavior(ProtocolProfileBehavior value) {
        this.protocolProfileBehavior = value;
    }

    @JsonProperty("request")
    public Request getRequest() {
        return request;
    }

    @JsonProperty("request")
    public void setRequest(Request value) {
        this.request = value;
    }

    @JsonProperty("response")
    public List<Object> getResponse() {
        return response;
    }

    @JsonProperty("response")
    public void setResponse(List<Object> value) {
        this.response = value;
    }

    @Override
    public String toString() {
        return (
            "Item{" +
            "name='" +
            name +
            '\'' +
            ", event=" +
            event +
            ", protocolProfileBehavior=" +
            protocolProfileBehavior +
            ", request=" +
            request +
            ", response=" +
            response +
            '}'
        );
    }
}
