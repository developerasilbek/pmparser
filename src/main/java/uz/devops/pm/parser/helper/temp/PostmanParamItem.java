package uz.devops.pm.parser.helper.temp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PostmanParamItem {

    private String name;
    private List<Item> item;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String value) {
        this.name = value;
    }

    @JsonProperty("item")
    public List<Item> getItem() {
        return item;
    }

    @JsonProperty("item")
    public void setItem(List<Item> value) {
        this.item = value;
    }

    @Override
    public String toString() {
        return "PostmanParamItem{" + "name='" + name + '\'' + ", item=" + item + '}';
    }
}
