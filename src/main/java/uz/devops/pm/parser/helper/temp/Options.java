package uz.devops.pm.parser.helper.temp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Options {

    private Raw raw;

    @JsonProperty("raw")
    public Raw getRaw() {
        return raw;
    }

    @JsonProperty("raw")
    public void setRaw(Raw value) {
        this.raw = value;
    }

    @Override
    public String toString() {
        return "Options{" + "raw=" + raw + '}';
    }
}
