package uz.devops.pm.parser.helper;

public class Requests {
    String requestName;

    public Requests() {
    }

    public Requests(String requestName) {
        this.requestName = requestName;
    }

    public String getRequestName() {
        return requestName;
    }

    public void setRequestName(String requestName) {
        this.requestName = requestName;
    }

    @Override
    public String toString() {
        return "Requests{" +
            "requestName='" + requestName + '\'' +
            '}';
    }
}
