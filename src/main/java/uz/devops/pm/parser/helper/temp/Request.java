package uz.devops.pm.parser.helper.temp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Request {

    private Auth auth;
    private String method;
    private List<Object> header;
    private Body body;
    private URL url;

    @JsonProperty("auth")
    public Auth getAuth() {
        return auth;
    }

    @JsonProperty("auth")
    public void setAuth(Auth value) {
        this.auth = value;
    }

    @JsonProperty("method")
    public String getMethod() {
        return method;
    }

    @JsonProperty("method")
    public void setMethod(String value) {
        this.method = value;
    }

    @JsonProperty("header")
    public List<Object> getHeader() {
        return header;
    }

    @JsonProperty("header")
    public void setHeader(List<Object> value) {
        this.header = value;
    }

    @JsonProperty("body")
    public Body getBody() {
        return body;
    }

    @JsonProperty("body")
    public void setBody(Body value) {
        this.body = value;
    }

    @JsonProperty("url")
    public URL getURL() {
        return url;
    }

    @JsonProperty("url")
    public void setURL(URL value) {
        this.url = value;
    }

    @Override
    public String toString() {
        return "Request{" + "auth=" + auth + ", method='" + method + '\'' + ", header=" + header + ", body=" + body + ", url=" + url + '}';
    }
}
