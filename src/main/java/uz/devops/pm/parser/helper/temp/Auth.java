package uz.devops.pm.parser.helper.temp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Auth {

    private String type;
    private List<Basic> bearer;
    private List<Basic> basic;

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String value) {
        this.type = value;
    }

    @JsonProperty("bearer")
    public List<Basic> getBearer() {
        return bearer;
    }

    @JsonProperty("bearer")
    public void setBearer(List<Basic> value) {
        this.bearer = value;
    }

    @JsonProperty("basic")
    public List<Basic> getBasic() {
        return basic;
    }

    @JsonProperty("basic")
    public void setBasic(List<Basic> value) {
        this.basic = value;
    }

    @Override
    public String toString() {
        return "Auth{" + "type='" + type + '\'' + ", bearer=" + bearer + ", basic=" + basic + '}';
    }
}
