package uz.devops.pm.parser.helper.temp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Raw {

    private String language;

    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("language")
    public void setLanguage(String value) {
        this.language = value;
    }

    @Override
    public String toString() {
        return "Raw{" + "language='" + language + '\'' + '}';
    }
}
