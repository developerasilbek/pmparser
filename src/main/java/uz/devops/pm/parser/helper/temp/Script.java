package uz.devops.pm.parser.helper.temp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Script {

    private List<String> exec;
    private String type;

    @JsonProperty("exec")
    public List<String> getExec() {
        return exec;
    }

    @JsonProperty("exec")
    public void setExec(List<String> value) {
        this.exec = value;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String value) {
        this.type = value;
    }

    @Override
    public String toString() {
        return "Script{" + "exec=" + exec + ", type='" + type + '\'' + '}';
    }
}
