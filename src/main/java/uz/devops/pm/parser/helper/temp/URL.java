package uz.devops.pm.parser.helper.temp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class URL {

    private String raw;
    private String protocol;
    private List<String> host;
    private String port;
    private List<String> path;
    private List<Query> query;

    @JsonProperty("raw")
    public String getRaw() {
        return raw;
    }

    @JsonProperty("raw")
    public void setRaw(String value) {
        this.raw = value;
    }

    @JsonProperty("protocol")
    public String getProtocol() {
        return protocol;
    }

    @JsonProperty("protocol")
    public void setProtocol(String value) {
        this.protocol = value;
    }

    @JsonProperty("host")
    public List<String> getHost() {
        return host;
    }

    @JsonProperty("host")
    public void setHost(List<String> value) {
        this.host = value;
    }

    @JsonProperty("port")
    public String getPort() {
        return port;
    }

    @JsonProperty("port")
    public void setPort(String value) {
        this.port = value;
    }

    @JsonProperty("path")
    public List<String> getPath() {
        return path;
    }

    @JsonProperty("path")
    public void setPath(List<String> value) {
        this.path = value;
    }

    @JsonProperty("query")
    public List<Query> getQuery() {
        return query;
    }

    @JsonProperty("query")
    public void setQuery(List<Query> value) {
        this.query = value;
    }

    @Override
    public String toString() {
        return (
            "URL{" +
            "raw='" +
            raw +
            '\'' +
            ", protocol='" +
            protocol +
            '\'' +
            ", host=" +
            host +
            ", port='" +
            port +
            '\'' +
            ", path=" +
            path +
            ", query=" +
            query +
            '}'
        );
    }
}
