package uz.devops.pm.parser.helper.temp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class Info {

    private UUID postmanID;
    private String name;
    private String schema;
    private String exporterID;

    @JsonProperty("_postman_id")
    public UUID getPostmanID() {
        return postmanID;
    }

    @JsonProperty("_postman_id")
    public void setPostmanID(UUID value) {
        this.postmanID = value;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String value) {
        this.name = value;
    }

    @JsonProperty("schema")
    public String getSchema() {
        return schema;
    }

    @JsonProperty("schema")
    public void setSchema(String value) {
        this.schema = value;
    }

    @JsonProperty("_exporter_id")
    public String getExporterID() {
        return exporterID;
    }

    @JsonProperty("_exporter_id")
    public void setExporterID(String value) {
        this.exporterID = value;
    }

    @Override
    public String toString() {
        return (
            "Info{" +
            "postmanID=" +
            postmanID +
            ", name='" +
            name +
            '\'' +
            ", schema='" +
            schema +
            '\'' +
            ", exporterID='" +
            exporterID +
            '\'' +
            '}'
        );
    }
}
