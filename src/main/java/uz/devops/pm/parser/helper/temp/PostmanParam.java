package uz.devops.pm.parser.helper.temp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PostmanParam {

    private Info info;
    private List<PostmanParamItem> item;

    @JsonProperty("info")
    public Info getInfo() {
        return info;
    }

    @JsonProperty("info")
    public void setInfo(Info value) {
        this.info = value;
    }

    @JsonProperty("item")
    public List<PostmanParamItem> getItem() {
        return item;
    }

    @JsonProperty("item")
    public void setItem(List<PostmanParamItem> value) {
        this.item = value;
    }

    @Override
    public String toString() {
        return "PostmanParam{" + "info=" + info + ", item=" + item + '}';
    }
}
