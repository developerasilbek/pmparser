package uz.devops.pm.parser.helper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.devops.pm.parser.domain.PmParser;
import uz.devops.pm.parser.domain.enumeration.MethodType;
import uz.devops.pm.parser.domain.enumeration.ModeType;
import uz.devops.pm.parser.domain.enumeration.ProtocolType;
import uz.devops.pm.parser.domain.enumeration.RequestType;
import uz.devops.pm.parser.helper.temp.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class PmParserHelper {

    private final Logger log = LoggerFactory.getLogger(PmParserHelper.class);

    public List<PmParser> gatewayParamFromPostmanCollection(MultipartHttpServletRequest multipartHttpServletRequest, String requests) throws IOException, JSONException {
        log.info("Request for parse Postman Collection!");
        Iterator<String> fileNames = multipartHttpServletRequest.getFileNames();
        if (fileNames.hasNext()) {
            MultipartFile file = multipartHttpServletRequest.getFile(fileNames.next());
            if (file == null) {
                log.info("file mavjud emas!");
                return null;
            }

            List<PmParser> pmParserList = new ArrayList<>();
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(file.getInputStream(), "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();

            String inputStr;
            while ((inputStr = streamReader.readLine()) != null) {
                responseStrBuilder.append(inputStr);
            }

            Gson gson = new GsonBuilder().create();
            JSONObject jsonObject = new JSONObject(responseStrBuilder.toString());

            PostmanParam postmanParam = gson.fromJson(jsonObject.toString(), PostmanParam.class);
            List<Requests> requestList = gson.fromJson(requests, new TypeToken<List<Requests>>() {
            }.getType());

            List<PostmanParamItem> postmanParamItemList = postmanParam.getItem();
            if (postmanParamItemList == null) {
                log.info("postman collection bo'sh!");
                return null;
            }

            items:
            for (
                PostmanParamItem postmanParamItem : postmanParamItemList) {
                PmParser pmParser = new PmParser();

                //default values
                pmParser.setTimeout(0);
                pmParser.setProtocol(ProtocolType.REST);
                pmParser.setRequestMediaType("application/json");
                pmParser.setResponseMediaType("application/json");
                pmParser.setActive(true);
                pmParser.setMode(ModeType.PROD);

                List<Item> itemList = postmanParamItem.getItem();
                if (itemList != null) {
                    for (Item item : itemList) {
                        String itemName = item.getName();
                        boolean contained = false;
                        for (Requests requaredRequest : requestList) {
                            String requestName = requaredRequest.getRequestName();
                            if (itemName.equals(requestName)) {
                                contained = true;
                                break;
                            }
                        }
                        if (!contained) {
                            continue items;
                        }
                        Request request = item.getRequest();
                        if (request != null) {
                            Auth requestAuth = request.getAuth();
                            String requestMethod = request.getMethod();
                            List<Object> requestHeader = request.getHeader();
                            Body requestBody = request.getBody();
                            URL requestURL = request.getURL();
                            if (requestAuth != null) {
                                String requestAuthType = requestAuth.getType();
                                if (requestAuthType.equals("bearer")) {
                                    List<Basic> bearer = requestAuth.getBearer();
                                    if (bearer != null) {
                                        for (Basic basic : bearer) {
                                            String token = basic.getValue();
                                            pmParser.setToken(token);
                                        }
                                    }
                                } else if (requestAuthType.equals("basic")) {
                                    List<Basic> basicList = requestAuth.getBasic();
                                    if (basicList != null) {
                                        for (Basic basic : basicList) {
                                            String key = basic.getKey();
                                            String value = basic.getValue();
                                            if (key.equals("password")) {
                                                pmParser.setPassword(value);
                                            } else if (key.equals("username")) {
                                                pmParser.setUsername(value);
                                            }
                                        }
                                    }
                                }
                            }
                            if (requestMethod != null) {
                                pmParser.setHttpMethodType(MethodType.valueOf(requestMethod));
                            }
                            if (requestHeader != null) {
                                pmParser.setHeaders(requestHeader.toString());
                            }
                            if (requestBody != null) {
                                pmParser.setBody(requestBody.toString());
                                String requestBodyMode = requestBody.getMode();
                                if (requestBodyMode.equals("formdata")) {
                                    List<Basic> formdata = requestBody.getFormdata();
                                    for (Basic basic : formdata) {
                                        String key = basic.getKey();
                                        if (key.equalsIgnoreCase("type")) {
                                            String value = basic.getValue();
                                            pmParser.setType(RequestType.valueOf(value.toUpperCase()));
                                        }
                                    }
                                }
                            }
                            if (requestURL != null) {
                                String raw = requestURL.getRaw();
                                pmParser.setUrl(raw);
                            }
                        } else {
                            log.info("request bo'sh!");
                        }
                        pmParserList.add(pmParser);
                    }
                }
            }
            return pmParserList;
        }
        return null;
    }
}
