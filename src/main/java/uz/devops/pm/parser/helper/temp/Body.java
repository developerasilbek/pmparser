package uz.devops.pm.parser.helper.temp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Body {

    private String mode;
    private List<Basic> formdata;
    private String raw;
    private Options options;

    @JsonProperty("mode")
    public String getMode() {
        return mode;
    }

    @JsonProperty("mode")
    public void setMode(String value) {
        this.mode = value;
    }

    @JsonProperty("formdata")
    public List<Basic> getFormdata() {
        return formdata;
    }

    @JsonProperty("formdata")
    public void setFormdata(List<Basic> value) {
        this.formdata = value;
    }

    @JsonProperty("raw")
    public String getRaw() {
        return raw;
    }

    @JsonProperty("raw")
    public void setRaw(String value) {
        this.raw = value;
    }

    @JsonProperty("options")
    public Options getOptions() {
        return options;
    }

    @JsonProperty("options")
    public void setOptions(Options value) {
        this.options = value;
    }

    @Override
    public String toString() {
        return "Body{" + "mode='" + mode + '\'' + ", formdata=" + formdata + ", raw='" + raw + '\'' + ", options=" + options + '}';
    }
}
