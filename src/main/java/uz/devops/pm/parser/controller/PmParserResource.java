package uz.devops.pm.parser.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.devops.pm.parser.service.dto.PmParserDTO;
import uz.devops.pm.parser.service.dto.RequestDTO;
import uz.devops.pm.parser.service.impl.PmParserServiceImpl;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class PmParserResource {

    private final Logger log = LoggerFactory.getLogger(PmParserResource.class);

    private static final String ENTITY_NAME = "bmmsPmParser";

    @Value("${pm-parser.name}")
    private String applicationName;

    private final PmParserServiceImpl pmParserService;


    public PmParserResource(
        PmParserServiceImpl pmParserService) {
        this.pmParserService = pmParserService;
    }

    @PostMapping("/pm-parser")
    public ResponseEntity<PmParserDTO> createPmParser(@RequestBody RequestDTO requestDTO) throws URISyntaxException {
        log.debug("REST request to save PmParser : {}", requestDTO);
        PmParserDTO result = pmParserService.save(requestDTO);
        return ResponseEntity
            .created(new URI("/api/pm-parser/" + result.getId()))
            .header(applicationName, ENTITY_NAME, result.getId().toString())
            .body(result);
    }

    @PostMapping("/file/check")
    public ResponseEntity<Boolean> checkFile(MultipartHttpServletRequest multipartHttpServletRequest) throws URISyntaxException {
        log.debug("REST request to check PostmanCollection : {}", multipartHttpServletRequest);
        Boolean isPostmanCollection = pmParserService.checkFile(multipartHttpServletRequest);
        return ResponseEntity
            .created(new URI("/api/file/check/" + isPostmanCollection))
            .header(applicationName, ENTITY_NAME, String.valueOf(isPostmanCollection))
            .body(isPostmanCollection);
    }

    @PostMapping("/pm-parser/postman")
    public ResponseEntity<List<PmParserDTO>> createPmParserPostman(
        @RequestParam("requests") String requests,
        MultipartHttpServletRequest multipartHttpServletRequest
    ) throws URISyntaxException, IOException, JSONException {
        log.debug("REST request to save PmParser : {}", multipartHttpServletRequest);
        List<PmParserDTO> result = pmParserService.save(multipartHttpServletRequest, requests);
        return ResponseEntity
            .created(new URI("/api//pm-parser/postman/" + result.size()))
            .header(applicationName, ENTITY_NAME, String.valueOf(result.size()))
            .body(result);
    }
}
