package uz.devops.pm.parser.controller.exceptions;

public class BadRequestAlertException extends Exception{
    public BadRequestAlertException(String defaultMessage, String entityName, String errorKey) {
        super(defaultMessage + " - " + entityName + " - " + errorKey);
    }
}
