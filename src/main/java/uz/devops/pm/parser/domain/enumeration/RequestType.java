package uz.devops.pm.parser.domain.enumeration;

public enum RequestType {
    CHECK,
    PAY,
    STATUS,
    CANCEL,
    DEPOSIT,
    RECONCILE,
}
