package uz.devops.pm.parser.domain.enumeration;

public enum ProtocolType {
    REST,
    WS,
    HESSIAN,
}
