package uz.devops.pm.parser.domain.enumeration;

public enum ModeType {
    TEST,
    DEV,
    PROD,
}
