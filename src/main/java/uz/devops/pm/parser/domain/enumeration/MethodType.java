package uz.devops.pm.parser.domain.enumeration;

public enum MethodType {
    GET,
    POST,
    PUT,
    PATCH,
    DELETE,
}
