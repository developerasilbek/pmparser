package uz.devops.pm.parser.service.mapper;

import org.mapstruct.*;
import uz.devops.pm.parser.domain.PmParser;
import uz.devops.pm.parser.service.dto.PmParserDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PmParserMapper {
    PmParser toEntity(PmParserDTO pmParserDTO);

    PmParserDTO toDto(PmParser pmParser);

    List<PmParser> toEntity(List<PmParserDTO> pmParserDTOList);

    List<PmParserDTO> toDto(List<PmParser> pmParserList);
}
