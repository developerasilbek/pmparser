package uz.devops.pm.parser.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.devops.pm.parser.domain.PmParser;
import uz.devops.pm.parser.helper.PmParserHelper;
import uz.devops.pm.parser.service.PmParserService;
import uz.devops.pm.parser.service.dto.PmParserDTO;
import uz.devops.pm.parser.service.dto.RequestDTO;
import uz.devops.pm.parser.service.mapper.RequestMapper;
import uz.devops.pm.parser.service.mapper.PmParserMapper;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

@Service(PmParserService.NAME)
@ConditionalOnProperty(
    prefix = "pm-parser",
    name = "simulate",
    havingValue = "false"
)
public class PmParserServiceImpl implements PmParserService {

    private final Logger log = LoggerFactory.getLogger(PmParserServiceImpl.class);

    private final PmParserMapper pmParserMapper;

    private final RequestMapper requestMapper;

    private final PmParserHelper pmParserHelper;

    public PmParserServiceImpl(PmParserMapper pmParserMapper, RequestMapper requestMapper, PmParserHelper pmParserHelper) {
        this.pmParserMapper = pmParserMapper;
        this.requestMapper = requestMapper;
        this.pmParserHelper = pmParserHelper;
    }

    @Override
    public PmParserDTO save(RequestDTO requestDTO) {
        log.debug("Request to save PmParser : {}", requestDTO);
        PmParser pmParser = requestMapper.toEntity(requestDTO);
        return pmParserMapper.toDto(pmParser);
    }

    @Override
    public List<PmParserDTO> save(MultipartHttpServletRequest multipartHttpServletRequest, String requests)
        throws IOException, JSONException {
            List<PmParser> pmParserList = pmParserHelper.gatewayParamFromPostmanCollection(multipartHttpServletRequest, requests);
            return pmParserMapper.toDto(pmParserList);
    }

    @Override
    public Boolean checkFile(MultipartHttpServletRequest multipartHttpServletRequest) {
        Iterator<String> fileNames = multipartHttpServletRequest.getFileNames();
        while (fileNames.hasNext()) {
            MultipartFile file = multipartHttpServletRequest.getFile(fileNames.next());
            if (file == null) {
                log.info("file mavjud emas!");
                return false;
            }
            String originalFilename = file.getOriginalFilename();
            if (originalFilename == null || !originalFilename.endsWith(".postman_collection.json")) {
                log.info("file postman collection emas!");
                return false;
            }
        }
        return true;
    }

}
