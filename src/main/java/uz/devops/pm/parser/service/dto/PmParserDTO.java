package uz.devops.pm.parser.service.dto;

import uz.devops.pm.parser.domain.enumeration.MethodType;
import uz.devops.pm.parser.domain.enumeration.ModeType;
import uz.devops.pm.parser.domain.enumeration.ProtocolType;
import uz.devops.pm.parser.domain.enumeration.RequestType;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PmParserDTO {

    private Long id;

    @Size(max = 512)
    private String url;

    private RequestType type;

    @Size(max = 32)
    private String username;

    @Size(max = 64)
    private String password;

    @Size(max = 512)
    private String token;

    @Size(max = 2048)
    private String body;

    @Size(max = 256)
    private String statusQuery;

    @Size(max = 256)
    private String messageQuery;

    @Size(max = 32)
    private String successCode;

    @Size(max = 512)
    private String callback;

    private Integer timeout;

    @Size(max = 1024)
    private String headers;

    @Size(max = 1024)
    private String dataQueries;

    private MethodType httpMethodType;

    private ProtocolType protocol;

    @Size(max = 64)
    private String requestMediaType;

    @Size(max = 64)
    private String responseMediaType;

    @NotNull
    private Boolean active;

    private ModeType mode;

    private String extIdQuery;

    private Boolean successOn2xx;

    public PmParserDTO() {
    }

    public PmParserDTO(Long id, String url, RequestType type, String username, String password, String token, String body, String statusQuery, String messageQuery, String successCode, String callback, Integer timeout, String headers, String dataQueries, MethodType httpMethodType, ProtocolType protocol, String requestMediaType, String responseMediaType, Boolean active, ModeType mode, String extIdQuery, Boolean successOn2xx) {
        this.id = id;
        this.url = url;
        this.type = type;
        this.username = username;
        this.password = password;
        this.token = token;
        this.body = body;
        this.statusQuery = statusQuery;
        this.messageQuery = messageQuery;
        this.successCode = successCode;
        this.callback = callback;
        this.timeout = timeout;
        this.headers = headers;
        this.dataQueries = dataQueries;
        this.httpMethodType = httpMethodType;
        this.protocol = protocol;
        this.requestMediaType = requestMediaType;
        this.responseMediaType = responseMediaType;
        this.active = active;
        this.mode = mode;
        this.extIdQuery = extIdQuery;
        this.successOn2xx = successOn2xx;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public RequestType getType() {
        return type;
    }

    public void setType(RequestType type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getStatusQuery() {
        return statusQuery;
    }

    public void setStatusQuery(String statusQuery) {
        this.statusQuery = statusQuery;
    }

    public String getMessageQuery() {
        return messageQuery;
    }

    public void setMessageQuery(String messageQuery) {
        this.messageQuery = messageQuery;
    }

    public String getSuccessCode() {
        return successCode;
    }

    public void setSuccessCode(String successCode) {
        this.successCode = successCode;
    }

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public String getHeaders() {
        return headers;
    }

    public void setHeaders(String headers) {
        this.headers = headers;
    }

    public String getDataQueries() {
        return dataQueries;
    }

    public void setDataQueries(String dataQueries) {
        this.dataQueries = dataQueries;
    }

    public MethodType getHttpMethodType() {
        return httpMethodType;
    }

    public void setHttpMethodType(MethodType httpMethodType) {
        this.httpMethodType = httpMethodType;
    }

    public ProtocolType getProtocol() {
        return protocol;
    }

    public void setProtocol(ProtocolType protocol) {
        this.protocol = protocol;
    }

    public String getRequestMediaType() {
        return requestMediaType;
    }

    public void setRequestMediaType(String requestMediaType) {
        this.requestMediaType = requestMediaType;
    }

    public String getResponseMediaType() {
        return responseMediaType;
    }

    public void setResponseMediaType(String responseMediaType) {
        this.responseMediaType = responseMediaType;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public ModeType getMode() {
        return mode;
    }

    public void setMode(ModeType mode) {
        this.mode = mode;
    }

    public String getExtIdQuery() {
        return extIdQuery;
    }

    public void setExtIdQuery(String extIdQuery) {
        this.extIdQuery = extIdQuery;
    }

    public Boolean getSuccessOn2xx() {
        return successOn2xx;
    }

    public void setSuccessOn2xx(Boolean successOn2xx) {
        this.successOn2xx = successOn2xx;
    }

    @Override
    public String toString() {
        return "PmParserDTO{" +
            "id=" + id +
            ", url='" + url + '\'' +
            ", type=" + type +
            ", username='" + username + '\'' +
            ", password='" + password + '\'' +
            ", token='" + token + '\'' +
            ", body='" + body + '\'' +
            ", statusQuery='" + statusQuery + '\'' +
            ", messageQuery='" + messageQuery + '\'' +
            ", successCode='" + successCode + '\'' +
            ", callback='" + callback + '\'' +
            ", timeout=" + timeout +
            ", headers='" + headers + '\'' +
            ", dataQueries='" + dataQueries + '\'' +
            ", httpMethodType=" + httpMethodType +
            ", protocol=" + protocol +
            ", requestMediaType='" + requestMediaType + '\'' +
            ", responseMediaType='" + responseMediaType + '\'' +
            ", active=" + active +
            ", mode=" + mode +
            ", extIdQuery='" + extIdQuery + '\'' +
            ", successOn2xx=" + successOn2xx +
            '}';
    }
}
