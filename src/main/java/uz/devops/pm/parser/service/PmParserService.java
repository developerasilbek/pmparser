package uz.devops.pm.parser.service;

import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.devops.pm.parser.service.dto.PmParserDTO;
import uz.devops.pm.parser.service.dto.RequestDTO;

import java.io.IOException;
import java.util.List;

@Service
public interface PmParserService {
    String NAME = "pmParserService";

    PmParserDTO save(RequestDTO requestDTO) throws IOException;

    List<PmParserDTO> save(MultipartHttpServletRequest multipartHttpServletRequest, String requests) throws IOException, JSONException;

    Boolean checkFile(MultipartHttpServletRequest multipartHttpServletRequest);

}
