package uz.devops.pm.parser.service.mapper;

import org.mapstruct.Mapper;
import uz.devops.pm.parser.domain.PmParser;
import uz.devops.pm.parser.service.dto.RequestDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RequestMapper {
    PmParser toEntity(RequestDTO requestDTO);

    RequestDTO toDto(PmParser pmParser);

    List<PmParser> toEntity(List<RequestDTO> requestDTOList);

    List<RequestDTO> toDto(List<PmParser> pmParserList);
}
