package uz.devops.pm.parser.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.devops.pm.parser.service.PmParserService;
import uz.devops.pm.parser.service.dto.PmParserDTO;
import uz.devops.pm.parser.service.dto.RequestDTO;

import java.io.IOException;
import java.util.List;

@Service(PmParserService.NAME)
@ConditionalOnProperty(
        prefix = "pm-parser",
        name = "simulate",
        havingValue = "true",
        matchIfMissing = true
)
public class DummyPmParserServiceImpl implements PmParserService {

    private final Logger log = LoggerFactory.getLogger(DummyPmParserServiceImpl.class);

    public DummyPmParserServiceImpl() {
        log.debug("############### PmParser simulation is ON ###############");
    }

    @Override
    public PmParserDTO save(RequestDTO requestDTO) throws IOException {
        return null;
    }

    @Override
    public List<PmParserDTO> save(MultipartHttpServletRequest multipartHttpServletRequest, String requests) throws IOException {
        return null;
    }

    @Override
    public Boolean checkFile(MultipartHttpServletRequest multipartHttpServletRequest) {
        return null;
    }

}
