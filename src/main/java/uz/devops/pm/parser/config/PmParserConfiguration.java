package uz.devops.pm.parser.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(PmParserProperties.class)
public class PmParserConfiguration {
}
